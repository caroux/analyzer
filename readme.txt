AnalyzeText  v1.0  2019/07/17
-----------------------------

Installation
------------

Java JDK1.8 minimum required
This has been built with Apache Maven 3.6.1 and Eclipse.
Maven is required to recompile the project, you have to install it. 
The environment variable JAVA_HOME has to be set to a JDK (ex: C:\Program Files\Java\jdk1.8.0_211).
Set also the environment variable MAVEN_HOME = D:\Program Files\apache-maven-3.6.1
Check your path contains the path to java jdk1.8 (only, or before any other JDK/JRE in the path).

Description
-----------

This project is intended to split text into words 
and order them with the following requirements:
 - each word is unique (different case makes different word) and its occurrence in the text is counted and displayed
 - first priority order is by ascending length
 - second priority order is ascending lexicographic order.
 

 As examples, you could run the following:
 
  0.> in a Git Bash shell, in the repository root folder, please use the command mvn clean install -U for compiling.
Then go to the directory target/classes, and run the commands below:

  1.> java AnalyzeText
results in :
	1 The
	1 fox
	1 the
	1 back
	1 lazy
	1 over
	2 brown
	1 dog's
	1 quick
	1 jumped

  2.> java AnalyzeText "abcd aabcd zz zztop zz abcd"
results in :
	2 zz
	2 abcd
	1 aabcd
	1 zztop

  3.> java AnalyzeText "abcd,aabcd,zz,zztop,zz,abcd" ","
results in :
	2 zz
	2 abcd
	1 aabcd
	1 zztop

	
The project provides also a Maven library that includes the packages text and text.analyzer.
The package text contains the Comparator interface and an implementation (see described above the sort order examples).
While the subpackage text.analyzer contains the Analyzer class that will split and sort the input text. 
Other split separators and comparators can be used.
The Analyzer class uses also the Node class to keep the number of occurrences of a word in the input text. 
This class has facilities to make or merge arrays, split text, etc.

This library can be used with the following dependency:

		<dependency>
			<groupId>text</groupId>
			<artifactId>analyzer</artifactId>
			<version>1.0</version>
		</dependency>
		
The Maven project can also be run with its JUnit5 test classes, including unit and integration tests. 
The tests do not cover all the code, but the main critical sections.

