import text.analyzer.Analyzer;
import text.analyzer.Node;

/**
 * This is the application class that takes 2 String arguments: the first String
 * is the text to analyze, while the second one is the Regular Expression that
 * defines the list of separators to split the text into words.
 *
 * @author carole
 */
public class AnalyzeText {
	public static void main(String[] args) {
		String text = (args.length > 0) ? args[0] : "The quick brown fox jumped over the lazy brown dog's back";
		String splitSeparators = (args.length > 1) ? args[1] : "\\s+";

		Analyzer analyzer = new Analyzer();
		Node[] words = analyzer.analyze(text, splitSeparators, null);

		for (Node node : words) {
			if (node != null)
				System.out.format("%d %s\n", node.getValue(), node.getItem());
		}
	}
}
