package text;

/**
 * This interface is aimed to provide a common way to compare 2 strings, as the Comparable<String> interface
 * Implementation instances of this interface can be provided to the BinaryTree class, for sorting and storing nodes.
 * 
 * @author carole
 *
 */
public interface Comparator {
	
	/**
	 * Almost as the compareTo method of the Comparable<string> interface, 
	 * this method returns 0 if the input strings are equal, 
	 * result < 0 if str1 < str2
	 * result > 0 if str1 > str2
	 * 
	 * @param str1 must be not null, otherwise NullPointerException is thrown
	 * @param str2 must be not null, otherwise NullPointerException is thrown
	 * @return
	 */
	int compare(String str1, String str2);

}
