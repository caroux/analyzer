package text;

/**
 * This class implements the Comparator interface in order to provide string order.
 * The order provided here is first by string length, then by ASCII order.
 * 
 * @author carole
 *
 */
public class LengthAndAsciiComparator implements Comparator {

	
	/**
	 * Almost as the compareTo method of the Comparable<string> interface, 
	 * this method returns 0 if the input strings are equal, 
	 * result < 0 if str1 < str2
	 * result > 0 if str1 > str2
	 * 
	 * @param str1 must be not null, otherwise NullPointerException is thrown
	 * @param str2 must be not null, otherwise NullPointerException is thrown
	 * @return
	 */
	public int compare(String str1, String str2) {
		int res = str1.length() - str2.length();
		if (res == 0) {
			return str1.compareTo(str2);
		}
		return res;
	}
}
