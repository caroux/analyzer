/**
 * 
 */
package text.analyzer;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.extern.slf4j.Slf4j;
import text.Comparator;
import text.LengthAndAsciiComparator;

/**
 * This class provides analysis for sentences through a statistical report of
 * words, sorted first by length then by their ASCii value.
 * 
 * @author carole
 */
@Slf4j
public class Analyzer {

	private static Logger log = Logger.getLogger(Analyzer.class.getName());

	/**
	 * This method first split the input text into words, thanks to the given
	 * regular expression regex, and sort it with the LengthAndAsciiComparator
	 * comparison order.
	 * 
	 * @param input      text to split, must not be null
	 * @param regex      regular expression, see Node.split as reference. must not
	 *                   be null.
	 * @param comparator can be null, default is LengthAndAsciiComparator.
	 * @return a sorted array of Node objects
	 */
	public Node[] analyze(String input, String regex, Comparator comparator) {
		Node[] words = Node.split(input, regex);
		if (words == null) {
			return null;
		}
		Comparator c = (comparator == null) ? new LengthAndAsciiComparator() : comparator;
		// didn't use a lambda expression for Comparator here in order to make a unit
		// test class for Comparator.
		Node[] sortedWords = sort(words, c);

		return sortedWords;
	}

	/**
	 * This method implements the merge sort algorithm.
	 * 
	 * @param words      the array of Node to sort
	 * @param comparator is the comparator used for the sort order
	 * @param begin      first index
	 * @param end        lastindex
	 * @return sorted array following the ascending order
	 */
	public Node[] sort(Node[] words, Comparator comparator) {
		if (log.isLoggable(Level.FINER)) {
			log.finer("sort: " + Arrays.toString(words));
		}
		if (words.length <= 1) {
			return words;
		}
		int p = findPivot(0, words.length - 1);
		return Node.merge(sort(Node.toSubArray(words, 0, p), comparator),
				sort(Node.toSubArray(words, p + 1, words.length - 1), comparator), comparator);
	}

	/**
	 * Returns the index of the pivot, located at the middle of the array
	 * 
	 * @param begin first index
	 * @param end   last index
	 * @return middle value between begin and end.
	 */
	int findPivot(int begin, int end) {
		return (begin + end) / 2;
	}

}
