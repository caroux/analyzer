package text.analyzer;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import lombok.extern.slf4j.Slf4j;
import text.Comparator;

/**
 * The Node class combines a String and an int value. It provides also array
 * operations, equality operation, copy (clone), and merge operations.
 * 
 * @author carole
 *
 */
@Slf4j
public class Node {

	private String item = "";
	private int value = 1;

	private static Logger log = Logger.getLogger(Node.class.getName());

	/**
	 * Default Node object has an empty string items and default int value equals to
	 * 1.
	 */
	public Node() {
	}

	/**
	 * Constructs a Node object with the input string item and default int value
	 * equals to 1.
	 * 
	 * @param item
	 */
	public Node(String item) {
		this.item = item;
	}

	/**
	 * Constructs a Node object with the input string item and a defined value.
	 * 
	 * @param str
	 * @param value
	 */
	public Node(String str, int value) {
		this.item = str;
		this.value = value;
	}

	/**
	 * Returns a copy of this node.
	 */
	@Override
	public Node clone() {
		return new Node(this.item, this.value);
	}

	/**
	 * Returns the string item contained in this Node object.
	 * 
	 * @return
	 */
	public String getItem() {
		return item;
	}

	/**
	 * Sets the string item for this Node object.
	 * 
	 * @param item
	 */
	public void setItem(String item) {
		this.item = item;
	}

	/**
	 * Returns the int value of this Node object.
	 * 
	 * @return
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets the int value of this Node object.
	 * 
	 * @param value
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * Increments the int value of this Node object by the input incr value.
	 * 
	 * @param incr int value as increment, can be negative, positive or equals to 0.
	 */
	public void increment(int incr) {
		this.value += incr;
	}

	/**
	 * Increments the int value of this node by 1.
	 * 
	 */
	public void increment() {
		this.value += 1;
	}

	/**
	 * This method breaks the input text into words, given the input separators.
	 * 
	 * @param text
	 * @param regex the regular expression for splitting the input text (reference:
	 *              String.split)
	 * @return an array of Node objects, each containing a word from the input text
	 *         (and default value 1).
	 */
	public static Node[] split(String text, String regex) {
		String[] words = text.split(regex);
		if (log.isLoggable(Level.FINER)) {
			log.finer("split: " + Arrays.toString(words));
		}
		return toArray(words);
	}

	/**
	 * Makes a copy of the input String array into an array of Node objects, with
	 * int value is 1 (by default).
	 * 
	 * @param items
	 * @return an array of Node objects, same length
	 */
	public static Node[] toArray(String[] items) {
		Node[] array = new Node[items.length];
		for (int i = 0; i < items.length; i++) {
			array[i] = new Node(items[i]);
		}
		return array;
	}

	/**
	 * Returns a new array with the input Node object inserted at first position.
	 * The Node are not cloned (this is not a deep copy).
	 * 
	 * @param node
	 * @param array
	 * @return a new array.
	 */
	public static Node[] toArray(Node node, Node[] nodes) {
		// first take care of null and empty conditions
		if (nodes == null || nodes.length == 0) {
			if (node == null) {
				return null;
			}
			Node[] array = new Node[1];
			array[0] = node;
			return array;
		}
		if (node == null) {
			Node[] array = new Node[nodes.length];
			System.arraycopy(nodes, 0, array, 0, nodes.length);
			return array;
		}
		Node[] array = new Node[nodes.length + 1];
		array[0] = node;
		System.arraycopy(nodes, 0, array, 1, nodes.length);
		return array;
	}

	/**
	 * Returns a new array with all the input nodes inserted in the same order as
	 * input.
	 * 
	 * @param nodes
	 * @return an array of the input Node objects
	 */
	public static Node[] toArray(Node... nodes) {
		return nodes;
	}

	/**
	 * return a subarray copy of the input Node array.
	 * 
	 * @param nodes
	 * @param begin start index
	 * @param end   last index
	 * @return an array of length = end - begin + 1
	 */
	public static Node[] toSubArray(Node[] nodes, int begin, int end) {
		int length = end - begin + 1;
		if (length <= 0) {
			return null;
		}
		Node[] array = new Node[length];
		System.arraycopy(nodes, begin, array, 0, length);
		return array;
	}

	/**
	 * Merge 2 arrays of Node objects, and count the occurrences of Node string
	 * items. The merge uses the sort order defined by the input comparator.
	 * 
	 * @param items1
	 * @param items2
	 * @param comparator
	 * @return an new array those length is lower or equal to the addition of the 2
	 *         input arrays.
	 */
	public static Node[] merge(Node[] items1, Node[] items2, Comparator comparator) {
		Node[] resultArray = null;
		if (items1 == null || items1.length == 0) {
			if (items2 != null && items2.length > 0) {
				Node[] copy = new Node[items2.length];
				System.arraycopy(items2, 0, copy, 0, items2.length);
				resultArray = copy;
			} // else arrayCopy = null
		} else if (items2 == null || items2.length == 0) {
			Node[] copy = new Node[items1.length];
			System.arraycopy(items1, 0, copy, 0, items1.length);
			resultArray = copy;
		} else {

			Node[] subarray;
			int result = comparator.compare(items1[0].getItem(), items2[0].getItem());
			if (result < 0) {
				if (items1.length > 1) {
					subarray = new Node[items1.length - 1];
					System.arraycopy(items1, 1, subarray, 0, items1.length - 1);
					resultArray = toArray(items1[0].clone(), merge(subarray, items2, comparator));
				} else {
					resultArray = toArray(items1[0].clone(), items2);
				}
			} else if (result > 0) {
				if (items2.length > 1) {
					subarray = new Node[items2.length - 1];
					System.arraycopy(items2, 1, subarray, 0, items2.length - 1);
					resultArray = toArray(items2[0].clone(), merge(items1, subarray, comparator));
				} else {
					resultArray = toArray(items2[0].clone(), items1);
				}
			} else {
				// case where we've found an occurrence of an existing element.
				Node n = items2[0].clone();
				n.increment(items1[0].getValue());
				if (items1.length > 1) {
					subarray = new Node[items1.length - 1];
					System.arraycopy(items1, 1, subarray, 0, items1.length - 1);
					if (items2.length > 1) {
						Node[] subarray2 = new Node[items2.length - 1];
						System.arraycopy(items2, 1, subarray2, 0, items2.length - 1);
						resultArray = toArray(n, merge(subarray, subarray2, comparator));
					} else {
						resultArray = toArray(n, subarray);
					}
				} else if (items2.length > 1) {
					subarray = new Node[items2.length - 1];
					System.arraycopy(items2, 1, subarray, 0, items2.length - 1);
					resultArray = toArray(n, subarray);
				} else {
					resultArray = toArray(n);
				}
			}
		}
		if (log.isLoggable(Level.FINER)) {
			log.finer("merge returns array = " + Arrays.toString(resultArray));
		}
		return resultArray;
	}

	/**
	 * return the string representation of this node with the following format:
	 * "<value> <item>"
	 */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(Integer.toString(value));
		return str.append(' ').append(item).toString();
	}

	/**
	 * Returns the hashCode of this object, used for equality and comparison.
	 * 
	 */
	@Override
	public int hashCode() { // eclipse generated method
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + value;
		return result;
	}

	/**
	 * Returns true of this Node is equal to the input Node object.
	 */
	@Override
	public boolean equals(Object obj) { // eclipse generated method
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (value != other.value)
			return false;
		return true;
	}

}
