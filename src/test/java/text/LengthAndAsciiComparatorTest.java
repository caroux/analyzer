/**
 * Provides the unit testing of the package text with JUnit 5.
 */
package text;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test class for comparator
 * 
 * @author carole
 *
 */
public class LengthAndAsciiComparatorTest {

	static final String bonbon = "bonbon";
	static final String bonbon1 = "bonbon1";
	static final String bambis = "bambis";
	static final String toto = "toto";
	static final String tatata = "tatata";

	@Test
	public void testCompareWithNull1() {
		Comparator comparator = new LengthAndAsciiComparator();

		assertThrows(NullPointerException.class, () -> comparator.compare(null, toto));
	}

	@Test
	public void testCompareWithNull2() {
		Comparator comparator = new LengthAndAsciiComparator();

		assertThrows(NullPointerException.class, () -> comparator.compare(toto, null));
	}

	@Test
	public void testCompareWithNulls() {
		Comparator comparator = new LengthAndAsciiComparator();

		assertThrows(NullPointerException.class, () -> comparator.compare(null, null));
	}

	
	@Test
	public void testCompareTotoAndToto() {
		Comparator comparator = new LengthAndAsciiComparator();

		int result = comparator.compare(toto, toto);
		assertTrue(result == 0);
	}

	@Test
	public void testCompareBonbonAndBonbon1() {
		Comparator comparator = new LengthAndAsciiComparator();

		int result = comparator.compare(bonbon, bonbon1);
		assertTrue(result < 0);

		result = comparator.compare(bonbon1, bonbon);
		assertTrue(result > 0);
	}

	@Test
	public void testCompareBonbonAndBambis() {
		Comparator comparator = new LengthAndAsciiComparator();
		int result = comparator.compare(bonbon, bambis);

		assertTrue(result > 0);
	}
	

	@Test
	public void testCompareTotoAndTata() {
		Comparator comparator = new LengthAndAsciiComparator();

		int result = comparator.compare(toto, tatata);
		assertTrue(result < 0);

		result = comparator.compare(tatata, toto);
		assertTrue(result > 0);
	}
	

	@Test
	public void testCompareTotoAndBambis() {
		Comparator comparator = new LengthAndAsciiComparator();

		int result = comparator.compare(toto, bambis);
		assertTrue(result < 0);

		result = comparator.compare(bambis, toto);
		assertTrue(result > 0);
	}
}
