/**
 * 
 */
package text.analyzer;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import text.Comparator;
import text.LengthAndAsciiComparator;

/**
 * Test class to help developing the Analyzer class, used more as integration
 * testing. It uses checks the final result we wanted at first and it checks
 * also the result with another comparator (lexicographic order).
 * 
 * @author carole
 *
 */
public class AnalyzerITTest {

	/**
	 * 
	 */
	@Test
	public void testAnalyze() {
		String input = "The quick brown fox jumped over the lazy brown dog’s back";
		String separator = " ";
		Analyzer analyzer = new Analyzer();
		Node[] result = analyzer.analyze(input, separator, null);
		testExpectedResult(result);
	}

	@Test
	public void testSort() {
		String[] items = { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "brown", "dog’s", "back" };
		Node[] words = Node.toArray(items);
		Comparator comparator = new LengthAndAsciiComparator();
		Analyzer analyzer = new Analyzer();

		Node[] result = analyzer.sort(words, comparator);
		testExpectedResult(result);
	}

	private void testExpectedResult(Node[] result) {
		assertEquals(10, result.length);
		assertAll(() -> assertEquals("The", result[0].getItem()), () -> assertEquals(1, result[0].getValue()));
		assertAll(() -> assertEquals("fox", result[1].getItem()), () -> assertEquals(1, result[1].getValue()));
		assertAll(() -> assertEquals("the", result[2].getItem()), () -> assertEquals(1, result[2].getValue()));
		assertAll(() -> assertEquals("back", result[3].getItem()), () -> assertEquals(1, result[3].getValue()));
		assertAll(() -> assertEquals("lazy", result[4].getItem()), () -> assertEquals(1, result[4].getValue()));
		assertAll(() -> assertEquals("over", result[5].getItem()), () -> assertEquals(1, result[5].getValue()));
		assertAll(() -> assertEquals("brown", result[6].getItem()), () -> assertEquals(2, result[6].getValue()));
		assertAll(() -> assertEquals("dog’s", result[7].getItem()), () -> assertEquals(1, result[7].getValue()));
		assertAll(() -> assertEquals("quick", result[8].getItem()), () -> assertEquals(1, result[8].getValue()));
		assertAll(() -> assertEquals("jumped", result[9].getItem()), () -> assertEquals(1, result[9].getValue()));
	}

	@Test
	public void testAnalyzeLexicalOrder() {
		String input = "The quick brown fox jumped over the lazy brown dog’s back";
		String separator = " ";
		Analyzer analyzer = new Analyzer();
		Node[] result = analyzer.analyze(input, separator, (String str1, String str2) -> {
			return str1.compareTo(str2);
		});
		testLexicalOrderResult(result);
	}

	@Test
	public void testSortLexicalOrder() {
		String[] items = { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "brown", "dog’s", "back" };
		Node[] words = Node.toArray(items);
		Analyzer analyzer = new Analyzer();

		Node[] result = analyzer.sort(words, (String str1, String str2) -> {
			return str1.compareTo(str2);
		});
		testLexicalOrderResult(result);
	}

	private void testLexicalOrderResult(Node[] result) {
		assertEquals(10, result.length);
		assertAll(() -> assertEquals("The", result[0].getItem()), () -> assertEquals(1, result[0].getValue()));
		assertAll(() -> assertEquals("back", result[1].getItem()), () -> assertEquals(1, result[1].getValue()));
		assertAll(() -> assertEquals("brown", result[2].getItem()), () -> assertEquals(2, result[2].getValue()));
		assertAll(() -> assertEquals("dog’s", result[3].getItem()), () -> assertEquals(1, result[3].getValue()));
		assertAll(() -> assertEquals("fox", result[4].getItem()), () -> assertEquals(1, result[4].getValue()));
		assertAll(() -> assertEquals("jumped", result[5].getItem()), () -> assertEquals(1, result[5].getValue()));
		assertAll(() -> assertEquals("lazy", result[6].getItem()), () -> assertEquals(1, result[6].getValue()));
		assertAll(() -> assertEquals("over", result[7].getItem()), () -> assertEquals(1, result[7].getValue()));
		assertAll(() -> assertEquals("quick", result[8].getItem()), () -> assertEquals(1, result[8].getValue()));
		assertAll(() -> assertEquals("the", result[9].getItem()), () -> assertEquals(1, result[9].getValue()));
	}

	@Test
	public void testSortSmallArray() {
		String[] items = { "the", "brown", "the" };
		Node[] words = Node.toArray(items);
		Analyzer analyzer = new Analyzer();

		Node[] result = analyzer.sort(words, (String str1, String str2) -> {
			return str1.compareTo(str2);
		});

		assertEquals(2, result.length);
		assertAll(() -> assertEquals("brown", result[0].getItem()), () -> assertEquals(1, result[0].getValue()));
		assertAll(() -> assertEquals("the", result[1].getItem()), () -> assertEquals(2, result[1].getValue()));
	}

	@Test
	public void testSortAllSameItems() {
		String[] items = { "the", "the", "the", "the", "the" };
		Node[] words = Node.toArray(items);
		Analyzer analyzer = new Analyzer();

		Node[] result = analyzer.sort(words, (String str1, String str2) -> {
			return str1.compareTo(str2);
		});

		assertEquals(1, result.length);
		assertAll(() -> assertEquals("the", result[0].getItem()), () -> assertEquals(5, result[0].getValue()));
	}

}
