/**
 * 
 */
package text.analyzer;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import text.LengthAndAsciiComparator;

/**
 * Test class to help developing the Node class.
 * 
 * @author carole
 *
 */
class NodeTest {

	/**
	 * Test method for {@link text.analyzer.Node#Node()}.
	 */
	@Test
	void testNode() {
		Node node = new Node();
		assertEquals("", node.getItem());
		assertEquals(1, node.getValue());
	}

	/**
	 * Test method for {@link text.analyzer.Node#Node(java.lang.String)}.
	 */
	@Test
	void testNodeString() {
		Node node = new Node("toto");
		assertEquals("toto", node.getItem());
		assertEquals(1, node.getValue());
	}

	/**
	 * Test method for {@link text.analyzer.Node#Node(java.lang.String, int)}.
	 */
	@Test
	void testNodeStringInt() {
		Node node = new Node("tata", -2);
		assertEquals("tata", node.getItem());
		assertEquals(-2, node.getValue());
	}

	/**
	 * Test method for {@link text.analyzer.Node#increment(int)}.
	 */
	@Test
	void testIncrementInt() {
		Node node = new Node("toto");
		node.increment(1);
		assertEquals(2, node.getValue());
		node.increment(-1);
		assertEquals(1, node.getValue());
	}

	/**
	 * Test method for {@link text.analyzer.Node#increment()}.
	 */
	@Test
	void testIncrement() {
		Node node = new Node("toto");
		node.increment();
		assertEquals(2, node.getValue());
	}

	/**
	 * Test method for
	 * {@link text.analyzer.Node#split(java.lang.String, java.lang.String)}.
	 */
	@Test
	void testSplit() {
		String text = "The quick brown fox jumped over the lazy brown dog’s back";
		String splitSeparators = "\\s+";
		Node[] words = Node.split(text, splitSeparators);
		assertEquals(11, words.length);
		assertEquals("The", words[0].getItem());
		assertEquals(1, words[0].getValue());
		assertEquals("back", words[words.length - 1].getItem());

		for (Node node : words) {
			if (node != null)
				System.out.format("%d %s\n", node.getValue(), node.getItem());
		}
	}

	/**
	 * Test method for {@link text.analyzer.Node#toArray(java.lang.String)}.
	 */
	@Test
	void testToArray() {
		String[] items = { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "brown", "dog’s", "back" };
		Node[] nodes = Node.toArray(items);
		assertEquals(items.length, nodes.length);
		assertEquals("The", nodes[0].getItem());
		assertEquals(1, nodes[0].getValue());
		assertEquals("back", nodes[nodes.length - 1].getItem());
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodeInputNull() {
		Node[] result = Node.toArray(null, null);

		assertNull(result);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodeNodeNull() {
		Node node = null;
		Node[] nodes = new Node[3];
		nodes[0] = new Node("toto");
		nodes[1] = new Node("tata");
		nodes[2] = new Node("tutu");
		Node[] result = Node.toArray(node, nodes);

		assertArrayEquals(nodes, result);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodeNodeArrayNull() {
		Node node = new Node("first", 2);
		Node[] nodes = null;
		Node[] result = Node.toArray(node, nodes);

		assertEquals(1, result.length);
		assertEquals(node, result[0]);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodeNodeArrayEmpty() {
		Node node = new Node("first", 2);
		Node[] nodes = new Node[0];
		Node[] result = Node.toArray(node, nodes);

		assertEquals(1, result.length);
		assertEquals(node, result[0]);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodeArrayCapacityNotNull() {
		Node node = new Node("first", 2);
		Node[] nodes = new Node[3];
		Node[] result = Node.toArray(node, nodes);

		assertEquals(4, result.length);
		assertEquals(node, result[0]);
		assertEquals(nodes[0], result[1]);
		assertEquals(nodes[1], result[2]);
		assertEquals(nodes[2], result[3]);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodeInput() {
		Node node = new Node("first", 2);
		Node[] nodes = new Node[3];
		nodes[0] = new Node("toto");
		nodes[1] = new Node("tata");
		nodes[2] = new Node("tutu");
		Node[] result = Node.toArray(node, nodes);

		assertEquals(4, result.length);
		assertEquals(node, result[0]);
		assertEquals(nodes[0], result[1]);
		assertEquals(nodes[1], result[2]);
		assertEquals(nodes[2], result[3]);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodes1Element() {
		Node node = new Node("first", 2);
		Node[] result = Node.toArray(node);

		assertEquals(1, result.length);
		assertEquals(node, result[0]);
	}

	/**
	 * test method for {@link text.analyzer.Node#toArray(text.analyzer.Node)}.
	 */
	@Test
	void testToArrayNodes2Elements() {
		Node node = new Node("first", 2);
		Node node1 = new Node("last", -1);
		Node[] result = Node.toArray(node, node1);

		assertEquals(2, result.length);
		assertEquals(node, result[0]);
		assertEquals(node1, result[1]);
	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}
	 */
	@Test
	void testMergeNodes2Null() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node, node1 };

		Node[] result = Node.merge(array1, null, new LengthAndAsciiComparator());
		assertArrayEquals(array1, result);

	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}
	 */
	@Test
	void testMergeNodes2Empty() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node, node1 };

		Node[] result = Node.merge(array1, new Node[0], new LengthAndAsciiComparator());
		assertArrayEquals(array1, result);

	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}
	 */
	@Test
	void testMergeNodes1Null() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node, node1 };
		Node[] n = null;

		Node[] result = Node.merge(n, array1, new LengthAndAsciiComparator());
		assertArrayEquals(array1, result);

	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}
	 */
	@Test
	void testMergeNodes1Empty() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node, node1 };

		Node[] result = Node.merge(new Node[0], array1, new LengthAndAsciiComparator());
		assertArrayEquals(array1, result);

	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}. Result
	 * should be [1 last, 1 first] as "first".length > "last".length
	 */
	@Test
	void testMergeNodesSingleElement() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node };
		Node[] array2 = { node1 };

		Node[] result = Node.merge(array2, array1, new LengthAndAsciiComparator());
		assertEquals(2, result.length);
		assertEquals(node1, result[0]);
		assertEquals(node, result[1]);
	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}. Result
	 * should be [1 last, 1 first] as "first".length > "last".length
	 */
	@Test
	void testMergeNodesSingleElement2() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node };
		Node[] array2 = { node1 };

		Node[] result = Node.merge(array1, array2, new LengthAndAsciiComparator());
		assertEquals(2, result.length);
		assertEquals(node1, result[0]);
		assertEquals(node, result[1]);
	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}. Result
	 * should be [1 last, 1 first] as "first".length > "last".length
	 */
	@Test
	void testMergeNodesArrayOf2Elements() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node, node1 };
		Node[] array2 = {};

		Node[] result = Node.merge(array1, array2, new LengthAndAsciiComparator());
		assertEquals(2, result.length);
		assertEquals(node, result[0]);
		assertEquals(node1, result[1]);
	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}. Result
	 * should be [1 last, 1 first] as "first".length > "last".length
	 */
	@Test
	void testMergeNodesArrays() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node node3 = new Node("list");
		Node[] array1 = { node1, node };
		Node[] array2 = { node3 };

		Node[] result = Node.merge(array1, array2, new LengthAndAsciiComparator());
		assertEquals(3, result.length);
		assertEquals(node1, result[0]);
		assertEquals(node3, result[1]);
		assertEquals(node, result[2]);
	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}. Result
	 * should be [1 last, 1 first] as "first".length > "last".length
	 */
	@Test
	void testMergeNodesArrays1Occurrence() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node node3 = new Node("list");
		Node node4 = new Node("first");
		Node[] array1 = { node1, node };
		Node[] array2 = { node3, node4 };

		Node[] result = Node.merge(array1, array2, new LengthAndAsciiComparator());
		assertEquals(3, result.length);
		assertEquals(node1, result[0]);
		assertEquals(node3, result[1]);
		node.increment();
		assertEquals(node, result[2]);
	}

	/**
	 * test method for
	 * {@link text.analyzer.Node#merge(Node[], Node[], text.Comparator)}. Result
	 * should be [1 last, 1 first] as "first".length > "last".length
	 */
	@Test
	void testMergeNodesArrayOf2Elements2() {
		Node node = new Node("first");
		Node node1 = new Node("last");
		Node[] array1 = { node, node1 };
		Node[] array2 = {};

		Node[] result = Node.merge(array2, array1, new LengthAndAsciiComparator());
		assertEquals(2, result.length);
		assertEquals(node, result[0]);
		assertEquals(node1, result[1]);
	}

	/**
	 * Test method for {@link text.analyzer.Node#toString()}.
	 */
	@Test
	void testToString() {
		Node node = new Node("toto");
		String result = node.toString();
		assertEquals("1 toto", result);
	}

}
